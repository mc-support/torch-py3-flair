#

FROM pytorch/pytorch:latest

RUN apt-get update && apt-get -y upgrade && apt-get -y install htop mc nano

RUN pip install jupyterlab flask seaborn sklearn folium pandas spacy gensim flair

RUN jupyter notebook --generate-config && echo "c.NotebookApp.quit_button = False" >> /root/.jupyter/jupyter_notebook_config.py

RUN mkdir data

WORKDIR /data

CMD ["jupyter", "lab", "--allow-root", "--notebook-dir=/data/", "--ip=0.0.0.0", "--port=8888"]
